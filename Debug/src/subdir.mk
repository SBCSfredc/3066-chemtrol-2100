################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/LCD_160x128.cpp \
../src/PCBA_2100.cpp \
../src/Uart.cpp \
../src/Utilitas.cpp \
../src/cr_cpp_config.cpp \
../src/cr_startup_lpc175x_6x.cpp \
../src/main.cpp 

C_SRCS += \
../src/crp.c 

OBJS += \
./src/LCD_160x128.o \
./src/PCBA_2100.o \
./src/Uart.o \
./src/Utilitas.o \
./src/cr_cpp_config.o \
./src/cr_startup_lpc175x_6x.o \
./src/crp.o \
./src/main.o 

C_DEPS += \
./src/crp.d 

CPP_DEPS += \
./src/LCD_160x128.d \
./src/PCBA_2100.d \
./src/Uart.d \
./src/Utilitas.d \
./src/cr_cpp_config.d \
./src/cr_startup_lpc175x_6x.d \
./src/main.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C++ Compiler'
	arm-none-eabi-c++ -D__NEWLIB__ -DDEBUG -D__CODE_RED -DCORE_M3 -D__USE_CMSIS=CMSISv2p00_LPC17xx -DCPP_USE_HEAP -D__LPC17XX__ -I"C:\Users\Fredc.FCARLIN\Documents\lpcxpresso\workspace\Chemtrol_2100\inc" -I"C:\Users\Fredc.FCARLIN\Documents\lpcxpresso\workspace\CMSISv2p00_LPC17xx\inc" -O0 -g3 -Wall -c -fmessage-length=0 -fno-builtin -ffunction-sections -fdata-sections -fno-rtti -fno-exceptions -mcpu=cortex-m3 -mthumb -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/cr_startup_lpc175x_6x.o: ../src/cr_startup_lpc175x_6x.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C++ Compiler'
	arm-none-eabi-c++ -D__NEWLIB__ -DDEBUG -D__CODE_RED -DCORE_M3 -D__USE_CMSIS=CMSISv2p00_LPC17xx -DCPP_USE_HEAP -D__LPC17XX__ -I"C:\Users\Fredc.FCARLIN\Documents\lpcxpresso\workspace\Chemtrol_2100\inc" -I"C:\Users\Fredc.FCARLIN\Documents\lpcxpresso\workspace\CMSISv2p00_LPC17xx\inc" -Os -g3 -Wall -c -fmessage-length=0 -fno-builtin -ffunction-sections -fdata-sections -fno-rtti -fno-exceptions -mcpu=cortex-m3 -mthumb -MMD -MP -MF"$(@:%.o=%.d)" -MT"src/cr_startup_lpc175x_6x.d" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/%.o: ../src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -D__NEWLIB__ -DDEBUG -D__CODE_RED -DCORE_M3 -D__USE_CMSIS=CMSISv2p00_LPC17xx -DCPP_USE_HEAP -D__LPC17XX__ -I"C:\Users\Fredc.FCARLIN\Documents\lpcxpresso\workspace\Chemtrol_2100\inc" -I"C:\Users\Fredc.FCARLIN\Documents\lpcxpresso\workspace\CMSISv2p00_LPC17xx\inc" -O0 -g3 -Wall -c -fmessage-length=0 -fno-builtin -ffunction-sections -fdata-sections -mcpu=cortex-m3 -mthumb -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


