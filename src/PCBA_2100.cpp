/*
 * PCBA_2100.cpp
 *
 *  Created on: Apr 19, 2018
 *      Author: Fredc
 */
#define D0                 (1<<19)
#define D1                 (1<<20)
#define D2                 (1<<21)
#define D3                 (1<<22)
#define D4                 (1<<23)
#define D5                 (1<<24)
#define D6                 (1<<25)
#define D7                 (1<<26)
#define DATA_SHIFT              3

#include "PCBA_2100.h"
//#include "core_cm3.h"

//#ifdef __USE_CMSIS
#include "LPC17xx.h"

//#endif
uint32_t timer_mark;

void PCBA_2100::IO_Init  ( )
   {
    SysTick_Config (SystemFrequency / 12000);     // Gen intpt every 1 ms from core_cm3.h
    // Init on-board LED as output
    //LPC_GPIO0->FIODIR |= 1 << 22;  // For LPC Link Board
    LPC_GPIO1->FIODIR |= 1 << 18;  // For PC2100
   }

void PCBA_2100::LED_DS22 (bool ONOFF)
    {
    if (ONOFF)  LPC_GPIO1->FIOSET = 1 << 18;  //ON
    else        LPC_GPIO1->FIOCLR = 1 << 18;  // OFF
    }



//void PCBA_2100::init (void)
//   {
 //  }
//-----------------------------------------------------------------
volatile uint32_t msTicks; // counter for 1ms SysTicks
extern "C"
    {
    void SysTick_Handler(void)
       {
	msTicks++;
       }
    }

int  PCBA_2100::Get_Timer_Tick (void)
    {
    return msTicks;
    }
//-----------------------------------------------------------------------

void PCBA_2100::LCDdriver_ConfigGPIOtoLCD (void)
{
#ifdef __CODE_RED
  LPC_GPIO1->FIOMASK &= ~(LCD_DATA_PINS); //..Data Bits 19-26 (or 18-25)
  LPC_GPIO2->FIOMASK &= ~(LCD_PORT2_DIRECTIONS);  //Set the mask
  LPC_GPIO2->FIODIR  |= LCD_PORT2_DIRECTIONS;     //Set the direction
  LPC_GPIO1->FIODIR  |= LCD_DATA_PINS;     //Set the direction
      //LPC_GPIO3->FIODIR |= LCD_PORT3_DIRECTIONS; // Not Used

    // Set GPIO outputs connected to LCD to default values
  lcd_out_en();    //SET LCD_BFR_DIR_PIN
  LPC_GPIO2->FIOSET = LCD_CSB_PIN;    // LCD_CSB_SET
  LPC_GPIO2->FIOSET = LCD_A0_PIN;     // LCD_A0_SET
    LPC_GPIO2->FIOSET = LCD_WR_PIN;     // LCD_WR_SET
    LPC_GPIO2->FIOSET = LCD_RD_PIN;     // LCD_RD_SET
    //LPC_GPIO3->FIOSET = LCD_RESB_PIN;   // LCD_RESB_SET- not used
    LPC_GPIO1->FIOCLR =  LCD_DATA_PINS;   // LCD_DATA_CL - data bus to zero
#endif
}
// ====================================================================
// Routine to introduce short delays required during LCD initialization
// =====================================================================
#ifdef __CODE_RED
void ms_delay (int n) __attribute__((noinline));
#endif

//void ms_delay(int n)
//   {
//   volatile int d;
//   for (d=0; d<(n*3000); d++){}
 //  }

#ifdef __CODE_RED
void qk_delay (int n) __attribute__((noinline));
#endif


void PCBA_2100::qk_delay(int n)
{
#ifdef __CODE_RED
   volatile int d;
   for (d=0; d<n; d++){
     __NOP();
   }
#endif
}


/** ===================================================================
 *  Routine to read status from the  LCD.
 *  Routine to read data from the  LCD. Normally called in combination
 *  with LCDdriver_WriteCom() routine
 *  ===================================================================
     unsigned char Read_LCD_Status(void)
    {
      char DataByte;

      LCD_BUS_IN(); //DDRA = 0x00; // Configure the bus for input.
       clear_A0();      //special case for status (CMD to read status only)
      clear_CS1();     //Chip Select
      clear_RD();      //Activate the buss
       NOP();
       NOP();
      DataByte = LCD_DATA_IN; //PINA; //get the data
      set_RD();        //Deactivate read
      set_CS1();       //Unselect the chip
      LCD_BUS_OUT(); //DDRA = 0xFF;     //Configure the bus for output.
      return DataByte;
     }
 *
 *     =============== =========================
 *  A0                X
 *     ===============
 *     =================+       +==============
 *  ChipSel             |       |
 *                      +=====S=+
 *     =================+       +==============
 *  RD                  |       |
 *                      +=====S=+
 *     =============== =========================
 *  WR                X
 *     ===============
 * ====================================================================
 */
#ifdef __CODE_RED
//unsigned char LCD_ReadStatus (void) __attribute__((noinline));
#endif


unsigned char PCBA_2100::LCD_ReadStatus (void)
 {
  unsigned char RetStatus;
  #ifdef __CODE_RED
  LPC_GPIO1->FIOSET = LCD_DATA_PINS;
  lcd_in_en(); //input...................set data direction in the buffer chip
  LPC_GPIO2->FIOSET = LCD_A0_PIN  | LCD_WR_PIN ; //....,    for status only
  qk_delay(2);
  LPC_GPIO2->FIOCLR = LCD_CSB_PIN | LCD_RD_PIN; //....set CS & RD low
  qk_delay(6);
  RetStatus=(char)((LPC_GPIO1->FIOPIN & LCD_DATA_PINS)>>(DATA_SHIFT+16)); //....Store status   //2-21-14 Hardware change: Rewire USB_UP_LED, beeper, and LCD pins
  qk_delay(2);
  LPC_GPIO2->FIOSET = LCD_CSB_PIN  | LCD_RD_PIN ;//reset control lines
  qk_delay(1);
  LPC_GPIO2->FIOCLR = LCD_A0_PIN;     //A0_SET.........back to data Mode
  qk_delay(1);
  return RetStatus;
#else
  return(0x23);
#endif
}



/**
 * ======================================================================
 * Routine to configure set LCD driver to accept particular command.
 * A call to this routine will normally be followed by a call
 * to LCDdriver_WriteData() to transfer appropriate parameters to driver.
 *
 *     =============== =========================
 *  A0                X
 *     ===============
 *     =================+       +==============
 *  ChipSel             |       |
 *                      +=====S=+
 *     =============== =========================
 *  RD                X
 *     ===============
 *     =================+       +==============
 *  WR                  |       |
 *                      +=====S=+
 * ======================================================================
 * To shift data to output lines (bits 19-26), left shift LCD_Command 3 bits.
 * Then use FIOSETH, which sets only the upper four bytes of FIOSET.
 * This provides the other 16 bits of left shift.
 * */
#ifdef __CODE_RED
//void LCD_WriteCom(unsigned char LCD_Command)  __attribute__((noinline));
#endif

void PCBA_2100::LCD_WriteCom (unsigned char LCD_Command)
   {
   unsigned short LclCmd;
#ifdef __CODE_RED

   //load the command
   lcd_out_en(); //................................. set buffer data direction
   LclCmd = (unsigned int)(LCD_Command<<DATA_SHIFT); //shift Command to output position
                 //2-21-14 Hardware change: Rewire USB_UP_LED, beeper, and LCD pins
  LPC_GPIO1->FIOCLR = LCD_DATA_PINS;
  LPC_GPIO1->FIOSETH = LclCmd;//.............................set output lines
  qk_delay(1);
  LPC_GPIO2->FIOSET = LCD_A0_PIN | LCD_RD_PIN;//.............set A0-RD line to cmd mode
  qk_delay(1);
  LPC_GPIO2->FIOCLR = LCD_CSB_PIN | LCD_WR_PIN;//..............set chip select
  qk_delay(6);
  LPC_GPIO2->FIOSET = LCD_CSB_PIN | LCD_WR_PIN;
  qk_delay(1);
#endif
}



/**
 * ====================================================================
 *  Routine to write data to LCD driver. Normally called in combination
 *  with LCDdriver_WriteCom() routine
 *
 *     ===============
 *  A0                X
 *     =============== =========================
 *     =================+       +==============
 *  ChipSel             |       |
 *                      +=====S=+
 *     =============== =========================
 *  RD                X
 *     ===============
 *     =================+       +==============
 *  WR                  |       |
 *                      +=====S=+
 * ====================================================================
 * To shift data to output lines (bits 19-26), left shift LCD_Command 3 bits.
 * Then use FIOSETH, which sets only the upper four bytes of FIOSET.
 * This provides the other 16 bits of left shift.
 * */
#ifdef __CODE_RED
//void LCD_WriteData(unsigned char LCD_Data) __attribute__((noinline));
#endif

void PCBA_2100::LCD_WriteData (unsigned char LCD_Data)
   {
   unsigned int LclData;
  #ifdef __CODE_RED
  //Setup the data
  lcd_out_en() //............................set data direction in the buffer
  #ifdef REVA
  LCD_Data = flip(LCD_Data);  //6-14-07 flip PORTA bits
  #endif
  LclData = (unsigned int)(LCD_Data<<DATA_SHIFT); //.....shift data to output position   //2-21-14 Hardware change: Rewire USB_UP_LED, beeper, and LCD pins
  LPC_GPIO1->FIOCLR = LCD_DATA_PINS;
  LPC_GPIO1->FIOSETH = LclData;//.............................set output lines
  qk_delay(1);
  LPC_GPIO2->FIOCLR = LCD_A0_PIN;//.............reset A0 line to Data mode
  LPC_GPIO2->FIOSET = LCD_RD_PIN;//.............set RD line to Data mode
  qk_delay(1);
  LPC_GPIO2->FIOCLR = LCD_CSB_PIN | LCD_WR_PIN;
  qk_delay(6);
  LPC_GPIO2->FIOSET = LCD_CSB_PIN | LCD_WR_PIN ;
#endif
}

/**
 * ====================================================================
 * Routine to read data from the  LCD. Normally called in combination
 * with LCDdriver_WriteCom() routine
 *
 *     ===============
 *  A0                X
 *     =============== ========================
 *     =================+       +==============
 *  ChipSel             |       |
 *                      +=====S=+
 *     =================+       +==============
 *  RD                  |       |
 *                      +=====S=+
 *     =============== ========================
 *  WR                X
 *     ===============
 * ===================================================================
 * To read data from input lines (bits 19-26), right shift data 3 bits.
 * Then use FIOPINH, which reads only from the upper four bytes of FIOPIN.
 * This provides the other 16 bits of right shift.
 * */
#ifdef __CODE_RED
unsigned char LCD_ReadData(void) __attribute__((noinline));
#endif

unsigned char PCBA_2100::LCD_ReadData (void)
{
  unsigned char RetData;
#ifdef __CODE_RED
  lcd_in_en() //.........................................set buffer direction
  // A0 is High
  LPC_GPIO2->FIOCLR = LCD_A0_PIN;//.................reset A0 line to Data mode
  LPC_GPIO2->FIOSET = LCD_WR_PIN;//.....................................set WR
  qk_delay(2);
  LPC_GPIO2->FIOCLR = LCD_CSB_PIN | LCD_RD_PIN; //..............reset CS-RD
  qk_delay(6);
  RetData=(char)((LPC_GPIO1->FIOPINH & LCD_DATA_PINS)>>DATA_SHIFT); //..Get Data   //2-21-14 Hardware change: Rewire USB_UP_LED, beeper, and LCD pins
  qk_delay(1);
  LPC_GPIO2->FIOSET = LCD_CSB_PIN | LCD_RD_PIN ;//........Set CS-RD
  #ifdef REVA
  RetData = flip(RetData);
  #endif
  return RetData;
#endif
}


/*

  static CBox Rx0_Ser_Buff;            // Character Buffers
  static CBox Tx0_Ser_Buff;            // Character Buffers
  static CBox Rx1_Ser_Buff;            // Character Buffers
  static CBox Tx1_Ser_Buff;            // Character Buffers

  static int  OneSecondTimer;         // 5ms down counter for tracking 1 second intervals
  //static bool OneSecondFlag;        // 1 = 10 sec over
  //static int  HalfSecondTimer;      // 5ms down counter for tracking 1 second intervals
  //static bool HalfSecondFlag;       // 1 = 10 sec over
  static int DisplayTimer;            // Display refresh flag
*/

