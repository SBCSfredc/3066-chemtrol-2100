/*
===============================================================================
 Name        : main.c
 Author      : $(author)
 Version     :
 Copyright   : $(copyright)
 Description : main definition
===============================================================================
*/

#ifdef __USE_CMSIS
#include "LPC17xx.h"

#endif

//#include <cr_section_macros.h>

#include "utilitas.h"
#include "LCD_160x128.h"
#include "PCBA_2100.h"

//#define SystemFrequency 12000000     // should be 120 000 000

// TODO: insert other include files here

// TODO: insert other definitions and declarations here

//-----------------------------------------------------------------
//volatile uint32_t msTicks; // counter for 1ms SysTicks
//extern "C"
//    {
//    void SysTick_Handler(void)
//       {
//	msTicks++;
//       }
//    }
//-------------------------------------------------------------------

int main(void)
   {
   Utilitas util;
   LCD_Display LCD;


   uint32_t timer_mark;
   volatile static int i = 0 ; // Force the counter to be placed into memory

   PCBA_2100::IO_Init  ( );		// Initialize IO
   util.init ( );

    // Enter an infinite loop, just incrementing a counter
    //-------------------------------------
    // Init SysTick
    //SysTick_Config (SystemFrequency / 12000);     // Gen intpt every 1 ms from core_cm3.h
    //LCDdriver_ConfigGPIOtoLCD( );
    //void LCD_WriteCom(unsigned char LCD_Command);

    LCD.lcd_init( );
    LCD.hdw_clear( );
    LCD.clear ( );

    LCD.set_cur_pos(2, 2);
    //LCD_WriteCom (unsigned char LCD_Command);
    //LCD_WriteData (unsigned char LCD_Data);
    //void write_lcd_data( u08 disp_data )
    //void write_str( char *disp_str, char *attr_str, unsigned char len )
    //             1234567890123456
    LCD.write_str("Hi to U, World! ", "             ", 16);

    // Init on-board LED as output
    // LPC_GPIO0->FIODIR |= 1 << 22;  // For LPC Link Board
    // LPC_GPIO1->FIODIR |= 1 << 18;  // For PC2100
    // PCBA_2100::IO_Init  ( );

    for ( ; ; )
       {
       //timer_mark =  USB_IRQn; //USB_IRQn is defined in lbc17xx.h
       //timer_mark = msTicks;                                   // Take timer snapshot
       timer_mark = PCBA_2100::Get_Timer_Tick ( );                                   // Take timer snapshot

       while(PCBA_2100::Get_Timer_Tick ( ) < (timer_mark + 12000));    // Wait until 100ms has passed
       // LPC_GPIO0->FIOCLR = 1 << 22;              // For LPC Link Board  Turn the LED off
       // LPC_GPIO1->FIOCLR = 1 << 18;              // For LPC Link Board  Turn the LED off
       PCBA_2100::LED_DS22 (false);

       timer_mark = PCBA_2100::Get_Timer_Tick ( );    // Take timer snapshot
       while (PCBA_2100::Get_Timer_Tick ( ) < (timer_mark + 12000));    // Wait until 100ms has passed
       // LPC_GPIO0->FIOSET = 1 << 22;              // For LPC Link Board Turn the LED on
       // LPC_GPIO1->FIOSET = 1 << 18;            // For PC2100  // For LPC Link Board Turn the LED on
       PCBA_2100::LED_DS22 (true);
       }



    //-----------------------------
    while(1) {
        i++ ;
    }
    return 0 ;
}
