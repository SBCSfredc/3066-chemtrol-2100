/*
 * types.h
 *
 *  Created on: Apr 17, 2018
 *      Author: Fredc
 */

#ifndef TYPES_H_
#define TYPES_H_
#ifdef __CODE_RED
// this should not be a symbol in a project....
#ifndef PACK_STRUCT_END // but sometimes it is...
#define PACK_STRUCT_END __attribute((packed))
#endif
#endif

//for GCC compatability
#define inb(A) A
#define outb(A,B) A = B
#define BV(A) (1<<A)
#define ifbit(A,B) (((A) & 1<<(B)) && 1)
#define sbi(A, B) (A) |= (1<<(B))
#define cbi(A, B) (A) &= ~(1<<(B))


// datatype definitions macros
typedef unsigned char  u08;
typedef   signed char  s08;
typedef unsigned short u16;
typedef   signed short s16;
#ifndef U32_DEFINED
#define U32_DEFINED
typedef unsigned long  u32;
#endif
typedef   signed long  s32;

typedef unsigned char   LPC_BOOL;

#define U08_MIN 0
#define U08_MAX 255
#define S08_MIN -128
#define S08_MAX 127
#define U16_MIN 0
#define U16_MAX 65535
#define S16_MIN -32768
#define S16_MAX 32767
#define U32_MIN 0
#define U32_MAX 4294967295
#define S32_MIN -2147483648
#define S32_MAX 2147483647

#ifndef FALSE
#define FALSE   (0)
#endif

#ifndef TRUE
#define TRUE    (1)
#endif

#endif /* TYPES_H_ */
