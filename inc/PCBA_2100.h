/*
 * PCBA_2100.h
 *
 *  Created on: Apr 19, 2018
 *      Author: Fredc
 */

#ifndef PCBA_2100_H_
#define PCBA_2100_H_

#define SystemFrequency 12000000     // should be 120 000 000
//-----------------------------------------------

#define LCD_DATA_PINS      (D0 | D1 | D2 | D3 | D4 | D5 | D6 | D7)
#define LCD_CSB_PIN            (1<<5)//(1<<13)
#define LCD_A0_PIN             (1<<8)// No change
#define LCD_WR_PIN             (1<<6)//(1<<11)
#define LCD_RD_PIN             (1<<7)//(1<<12)
#define LCD_BFR_DIR_PIN      (1<<11)//(1<<12)

#define LCD_READ  0
#define LCD_WRITE  1
#define lcd_out_en() (LPC_GPIO2->FIOSET=LCD_BFR_DIR_PIN);//sbi(IO1SET,25)
#define lcd_in_en()  (LPC_GPIO2->FIOCLR=LCD_BFR_DIR_PIN);//sbi(IO1CLR,25)

#define LCDPORT    PORTD
#define LCDDATAout PORTA
#define LCDDATAin  PINA
#define LCDDATAddr DDRA

#define RD 7
#define WR 6
#define DSEL 5
#define A12 4

#define LCD_PORT2_DIRECTIONS  (LCD_CSB_PIN | LCD_A0_PIN | LCD_WR_PIN | LCD_RD_PIN | LCD_BFR_DIR_PIN)
//-------------------------------------------
class PCBA_2100
   {
   public:
      //void init (void);
      static void IO_Init (void);            // Initialize IO
      static void LED_DS22 (bool ONOFF);
      static int  Get_Timer_Tick (void);

      static void LCDdriver_ConfigGPIOtoLCD (void);
      static unsigned char LCD_ReadStatus (void);
      static void LCD_WriteCom (unsigned char LCD_Command);
      static unsigned char LCD_ReadData (void);
      static void LCD_WriteData (unsigned char LCD_Data);


   private:
      static void hdw_clear_extra (void);
      static void qk_delay (int n);
   };



#endif /* PCBA_2100_H_ */
















/*
 #include "Config.h"

enum Spi_Command    {SPI_READ, SPI_WRITE};
enum Digital_Output {DOUT_ALARM, DOUT_SAN_FEED, DOUT_PH_FEED, DOUT_BUZZER};


//enum Digital_Input  {DIN_FLOW, DIN_PADDLE};
//enum Analog_Output  {AOUT_PH, AOUT_ORP, AOUT_AUX};
//enum Analog_Input {AIN_AUX};       // doesn't work
// III:130
#define BTN_NO_PUSH    0x000

#define SPI_BUFF_MAX 10

#define RX0_BUFF_SIZE 100
#define TX0_BUFF_SIZE 200
#define RX1_BUFF_SIZE 25            // Holders for 422 section
#define TX1_BUFF_SIZE 25

#define RX_LINE_BUFF_SIZE 50

class PCBA_CH250K // :  public IO
   {
   public:
      static void IO_Init (void);            // Initialize IO

      static void Digital_Out (Digital_Output DOut, bool Set_On);  //{DOUT_ALARM, DOUT_SAN_FEED, DOUT_PH_FEED, DOUT_BUZZER};
                                             // III:139
      static bool CON2_Get ( );              // Jumper CON2

      static void RS232_Init (int Baud);     // RS 232
      static void RS232_Check_Tx (void);     // Update status of send.
      static void RS232_Send (char *ptr);    // Send a line
      static void RS232_Send (char ch);      // Send a char

      static char RS232_Check_Rx ( );        // Returns next char or null

      static void RS485_Init (int Baud);         // RS 485

      static bool Line1_Ready (void);           // Assembles characters into line. Returns true when ready
      //static char Get_Line1 (void);            // Returns char.
      static char *Get_Line1 (void);            // Returns char.

      //static bool Button_Read (Button btn);  //
      static void Buttons_Init (void);        // Initialize buttons

      static int  Button_Array (void);        // Get button array III:130
      static void Button_Check (void);
      static int  Paddle_Check (void);
      //static bool Button_Pressed (Button btn); // Checks to see if button pressed
      // LEDs III:145, 149
      //static void LED (LED_Output Led, bool Turn_On);

      static void Spi_Init     (void);        // Spi Init
      static int  Spi_Read_pH  (void);
      static int  Spi_Read_ORP (void);
      static int  Read_Flow    (void);

      //static void PWM_Init (void);                    // Initialize IO
      //static void PWM_Set (int pwm);                  //

      //static void ADC_Init (void);                    // Initialize IO
      //static void ADC_Start (int chan, bool High_Range);
      //static bool ADC_Done ( );
      //static int  ADC_Read ( );

      static void Timer_Init (void);                  // Initialize timer
      static bool Get_Timer_Tick (void);              // 2 ms Tick
      static bool Get_Timer_Flag (void);              // One second Timer Flag
      static bool Get_Display_Flag (void);            // Display Timer Flag
      static bool Get_Button_Flag (void);             // Button Timer Flag

      static void WatchDog_Init  (void);              // III:138
      static void WatchDog_Touch (void);
      static void WatchDog_Off   (void);


      //static void E10_Set (bool b);                // Initialize IO

      //static int Get_Bozo (int Bozo);                //

     static void Reboot  (void);

   protected:

   private:
      static int Button_Array_Raw;
      static int Button_Array_Old;
      static bool Line1_rdy;

      static char Line1 [RX_LINE_BUFF_SIZE];

      static bool RS232_Tx_Ready (void);
      static void RS232_Put_Char (char ch);

      static char RS232_Get_Char (void);
      static bool RS232_Rx_Ready (void);
      static char get_checksum (char *p, char cnt);
      static void sdly ( );                           // Spi delay

      static bool Spi_Ready (void);
      static char Spi_Send (char cData);                //Spi_Command    {SPI_READ, SPI_WRITE};
      static char Spi_Transmit (Spi_Command SPI_C, int Spi_Address, int Spi_Offset, int count);
      static char Spi_Get (int index);
      static void Spi_Put (int chan1, int chan2, int chan3);
   };
 */
