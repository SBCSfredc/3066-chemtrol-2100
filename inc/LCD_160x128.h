/*
 * LCD_160x128.h
 *
 *  Created on: Apr 17, 2018
 *      Author: Fredc
 */

#ifndef LCD_160X128_H_
#define LCD_160X128_H_

//#include <iom162v.h>
#include "types.h"


// Move these into class privatize as reqd

class LCD_Display
    {
    public:
	void lcd_init  (void);
	void hdw_clear (void);
	void clear (void);

	void write_lcd_data (u08 disp_data );
	void write_lcd_cmnd (u08 Cmd_data );
	u08 read_lcd_status (void);

	void LCDdriver_ConfigGPIOtoLCDxx (void);

	void set_cur_pos (unsigned char row, unsigned char col);
	void write_str (char *disp_str, char *attr_str, unsigned char len);

    protected:

    private:
	void hdw_clear_extra (void);
	void get_cur_pos (u08 *row, u08 *column );
	void clear2 (void);
	void update_display( void );
	void qk_delay(int n);
	//void write_str (char *disp_str, char *attr_str, unsigned char len );
	void message (u08 row, u08 col, u08 *string );

    };



#endif /* LCD_160X128_H_ */
